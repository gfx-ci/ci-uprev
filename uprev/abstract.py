#!/usr/bin/env python3.11

# Copyright (C) 2023-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023-2024 Collabora Ltd"

from enum import Enum, auto
from logging import getLogger
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import Any

logger = getLogger(__name__)


class Singleton:
    _instance: Any = None

    def __new__(cls, *args: Any, **kwargs: Any) -> Any:
        if cls._instance is None:
            new_instance = super(Singleton, cls).__new__(cls, *args, **kwargs)
            logger.debug("Created a fresh singleton instance of '%s'", cls.__name__)
            cls._instance = new_instance
        else:
            logger.debug("'%s' singleton requested", cls.__name__)
        return cls._instance

    @classmethod
    def singleton_clean(cls) -> None:
        logger.debug("Destroyed the '%s' singleton instance", cls.__name__)
        cls._instance = None


class KeyedSingleton:
    _instances: dict[str, Any] | None = None

    def __new__(cls, name: str, *args: Any, **kwargs: Any) -> Any:
        if cls._instances is None:
            cls._instances = {}
        if name not in cls._instances:
            new_instance = super(KeyedSingleton, cls).__new__(cls, *args, **kwargs)
            logger.debug(
                "Created a fresh singleton instance of '%s(%s)'", cls.__name__, name
            )
            cls._instances[name] = new_instance
        else:
            logger.debug("'%s(%s)' singleton requested", cls.__name__, name)
        return cls._instances[name]

    @classmethod
    def singleton_clean(cls, name: str) -> None:
        if (
            cls._instances is None
            or not isinstance(cls._instances, dict)
            or name not in cls._instances.keys()
        ):
            raise KeyError(f"{name} is not a keyed singleton of {cls.__name__}")
        logger.debug("Destroyed the '%s(%s)' singleton instance", cls.__name__, name)
        cls._instances.pop(name)


class JobStageGroup(Enum):
    build = auto()
    test = auto()
    other = auto()
