#!/usr/bin/env python3.11

# Copyright (C) 2022-2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from __future__ import annotations

__author__ = "Tomeu Vizoso and Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2022-2024 Collabora Ltd"

import base64
import os
import re
import sys
import time
from argparse import ArgumentParser, Namespace
from collections import OrderedDict, defaultdict, deque
from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import wait as wait_threads_pool
from copy import deepcopy
from datetime import datetime, timedelta, timezone
from functools import cache
from logging import DEBUG, ERROR, INFO, FileHandler, Formatter, StreamHandler, getLogger
from pathlib import Path
from pprint import pformat
from shutil import rmtree
from statistics import mean
from tempfile import TemporaryDirectory
from typing import TYPE_CHECKING, Optional, Tuple, Union
from zipfile import ZipFile

import colorlog
import git
from gitlab.base import RESTObject, RESTObjectList
from gitlab.exceptions import GitlabGetError, GitlabJobRetryError, GitlabUpdateError
from gitlab.v4.objects import (
    Project,
    ProjectBranch,
    ProjectCommit,
    ProjectIssue,
    ProjectIssueNote,
    ProjectJob,
    ProjectMergeRequest,
    ProjectPipeline,
)
from tenacity import after_log, retry, stop_after_attempt, wait_exponential

from .abstract import JobStageGroup
from .environment import (
    Branch,
    Dependency,
    Fork,
    GitlabProxy,
    LocalClone,
    Target,
    Template,
    Title,
    UpdateRevisionPair,
    Working,
    get_target_project,
    in_production,
)
from .exceptions import NothingToUprev
from .expectations import mesa_expectations_paths, virglrenderer_expectations_paths
from .uprevs import (
    UpdateRevision,
    UprevActionProcessor,
    uprev_mesa_in_virglrenderer,
    uprev_piglit_in_mesa,
)

if TYPE_CHECKING:
    from typing import Any

logger = getLogger(__name__)

# GLOBAL constants

LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE: int = 10
MAX_LINES_PER_CATEGORY_IN_NOTE: int = 100

CHECKER_SLEEP_TIME: int = 10  # seconds
PIPELINE_CHECKER_SLEEP_TIME: int = CHECKER_SLEEP_TIME * 6  # 1 minute
GL_OBJ_CREATION_TIMEOUT: int = 600  # seconds, 10 minutes

FAILED_JOB_RETRIES: int = 2


def defaultdictdeque() -> dict[str, deque[str]]:
    return defaultdict(deque)


def get_candidate_for_uprev(
    revision: str | None,
    pipeline_id: int | None,
) -> tuple[str, int]:
    """
    Depending on the information provided, find the best uprev candidate. If no restriction
    is set, get the last pipeline and its commit hash. If one of those two filters is set,
    find the corresponding value to pair them. If both are set, check that they are a valid pair.
    :param revision: to fix the revision to uprev to
    :param pipeline_id: to fix the pipeline, so its commit, to uprev to
    :return: commit hash and pipeline id
    """
    dep_project = Dependency().gl_project
    # (revision, pipeline_id) = (None, None): search the last
    if revision is None and pipeline_id is None:
        pipelines = __get_pipelines()
        pipeline = __select_pipeline(pipelines)
        if not pipeline:
            raise LookupError("No uprev candidate found")
        logger.info(
            "Found pipeline %d with SHA %s %s",
            pipeline.id,
            pipeline.sha,
            dep_project.commits.get(pipeline.sha).web_url,
        )
        return str(pipeline.sha), int(pipeline.id)
    # (revision, pipeline_id) = (None, value)
    elif revision is None and pipeline_id is not None:
        revision = __get_pipeline_commit(pipeline_id)
        logger.info(
            "Specified the pipeline to uprev to %d and found the revision %s %s",
            pipeline_id,
            revision,
            dep_project.commits.get(revision).web_url,
        )
        return revision, pipeline_id
    # (revision, pipeline_id) = (value, None)
    elif revision is not None and pipeline_id is None:
        pipeline_id = __search_pipeline(revision)
        logger.info(
            "Specified the revision to uprev to %s and found the pipeline %d %s",
            revision,
            pipeline_id,
            dep_project.commits.get(revision).web_url,
        )
        return revision, pipeline_id
    # (revision, pipeline_id) = (value, value)
    elif revision is not None and pipeline_id is not None:
        if pipeline_id != __search_pipeline(revision):
            raise AssertionError(
                f"Specified revision {revision}, and a pipeline id {pipeline_id} which doesn't corresponds"
            )
        logger.info(
            "Specified the revision to uprev to %s and the pipeline %d, which is correct %s",
            revision,
            pipeline_id,
            dep_project.commits.get(revision).web_url,
        )
        return revision, pipeline_id
    else:
        raise TypeError(
            "Inappropriate types in revision and pipeline to find an uprev candidate"
        )


def __get_pipelines(
    status: str = "success", username: str = "marge-bot"
) -> RESTObjectList | list[RESTObject]:
    """
    To update the revision, the first requisite is a pipeline that:
    1. It has to have succeeded,
    2. It has to be merged by marge
    :return: iterator over pipelines
    """
    dependency_project = Dependency().gl_project
    return dependency_project.pipelines.list(
        status=status,
        username=username,
        ordered_by="created_at",
        sort="desc",
        iterator=True,
    )


def __select_pipeline(
    pipelines: RESTObjectList | list[RESTObject],
) -> Optional[ProjectPipeline]:
    """
    Specific condition that the pipeline candidate must satisfy. In the case of
    mesa for virglrenderer it is necessary (because it uses some artifacts from
    the merge pipeline) to have the 'debian-testing' job.
    :return: the pipeline to use in the uprev.
    """
    uprev_pair = UpdateRevisionPair().enum
    for _pipeline in pipelines:
        pipeline = __get_pipeline(_pipeline.id, Dependency().gl_project)
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            debian_testing_job = [
                job
                for job in pipeline.jobs.list(iterator=True)
                if job.name == "debian-testing"
            ]
            if debian_testing_job:
                return pipeline
        else:
            return pipeline
    return None


def get_templates_commit(revision: str, filename: str = ".gitlab-ci.yml") -> str:
    """
    Find in a file of a gitlab project the definition of the
    MESA_TEMPLATES_COMMIT hash to be used.
    :param revision: define with commit, tag, branch use to read the file
    :param filename: file of the project where the variable is defined
    :return: hash of ci-templates to use in the uprev
    """
    template_project = Template().gl_project
    if template_project is None:
        raise ValueError("Template project singleton not initialized")
    file = template_project.files.get(filename, ref=revision)
    file_content = base64.b64decode(file.content).decode("utf-8")
    for line in file_content.split("\n"):
        # sample line '    MESA_TEMPLATES_COMMIT: &ci-templates-commit <hash>'
        # and we are interested in the hash at the end
        pattern = r"MESA_TEMPLATES_COMMIT:.* ([0-9a-fA-F].*)$"
        if search_result := re.search(pattern, line):
            return search_result.group(1)
    raise AttributeError(f"MESA_TEMPLATES_COMMIT not found in {filename}")


def create_branch(
    pipeline_id: int,
    revision: str,
    templates_commit: str,
) -> git.repo:  # type: ignore
    """
    With the uprev information, clone locally the target project and prepare
    the temporal working branch from the target's default branch. Then do the
    uprev itself and commit it.
    :param pipeline_id: reference pipeline of the dep project to uprev
    :param revision: dep project revision to uprev
    :param templates_commit: ci-templates commit to uprev
    :return: git repository object
    """
    repo = __clone_repo(force_fresh_repo=False)
    __clean_previous_tmp_branch()

    os.chdir(repo.working_dir)  # type: ignore
    try:
        amend, revision_in_use = __checkout_wip_branch(repo)
        __do_uprev(
            repo,
            pipeline_id,
            revision,
            templates_commit,
            amend_previous_uprev=amend,
            inject_old_revision=revision_in_use,
        )
    finally:
        os.chdir("..")

    return repo


def __clone_repo(
    force_fresh_repo: bool,
) -> git.repo:  # type: ignore
    """
    Prepare a local clone of the repository with the target project and its
    fork. It can reuse or clean a previous clone. And in case of clone from
    scratch, setup the git config.
    :param force_fresh_repo: flag what to do if the local clone already exists
    :return:
    """
    local_clone = LocalClone().directory
    target_project = get_target_project().gl_project
    if force_fresh_repo:
        if os.path.exists(local_clone):
            logger.warning("Remove previously existing repo in %s", local_clone)
            rmtree(local_clone)
    if os.path.exists(local_clone):
        logger.warning("Reusing repo in %s", local_clone)
        repo = git.Repo(local_clone)
        repo.remote("origin").update()
        repo.remote("fork").update()

        tmp_branch = Branch().for_wip
        if tmp_branch in repo.heads:
            logger.warning("Deleting existing branch")
            repo.heads[target_project.default_branch].checkout(force=True)
            repo.delete_head(tmp_branch, force=True)
    else:
        gl_user = GitlabProxy().gl_obj.user
        username = gl_user.username if gl_user is not None else ""
        useremail = gl_user.email if gl_user is not None else ""
        token = GitlabProxy().token
        project_path = get_target_project().path_with_namespace
        fork_path = Fork().path_with_namespace

        logger.info("Cloning repo at %s", local_clone)
        repo_url = f"https://gitlab.freedesktop.org/{project_path}.git"
        repo = git.Repo.clone_from(
            repo_url, local_clone, branch=target_project.default_branch
        )
        repo.config_writer().set_value(
            "user", "name", "Collabora's Gfx CI Team"
        ).release()
        repo.config_writer().set_value("user", "email", useremail).release()
        fork_url = f"https://{username}:{token}@gitlab.freedesktop.org/{fork_path}.git"
        repo.create_remote("fork", url=fork_url)
        repo.remotes["fork"].fetch()
    return repo


def __clean_previous_tmp_branch() -> None:
    """
    The temporary branch could still be in the fork repo because there is
    another uprev procedure in progress or because one failed lefting it
    orphan. So, when it is orphan a newer uprev procedure could fail and
    requires it to be cleaned.
    :return:
    """
    uprev_tmp_branch = Branch().for_wip
    if (branch := __get_branch(uprev_tmp_branch)) is None:
        return
    logger.warning("The branch %s exists in the fork", uprev_tmp_branch)
    __wait_for_running_pipelines_in_branch(branch)
    logger.warning("Remove orphan %s branch", uprev_tmp_branch)
    branch.delete()


def __checkout_wip_branch(repo: git.repo) -> Tuple[bool, str]:  # type: ignore
    """
    This method points the git clone to the Work In Progress (WIP) branch to
    prepare an uprev candidate. If there is a previous uprev proposal, it takes
    the branch to start the new proposal from here and rebase.
    It returns a boolean to report if the new uprev has to amend the last
    commit or it has to create a fresh one.
    :param repo: the object managing the git clone
    :return: if the WIP branch already has the uprev commit
    """
    target_project = get_target_project().gl_project
    fork_project = Fork().gl_project
    merge_request = __get_uprev_merge_request(Title().for_merge_request)
    if merge_request is not None:
        repo_git = repo.git  # type: ignore
        repo_head = repo.head  # type: ignore
        logger.info(
            "There is a previous uprev proposal (merge request %s) with "
            "branch %s, so the current uprev attempt will start from rebasing it.",
            merge_request.web_url,
            merge_request.source_branch,
        )
        old_revision = __check_dependency_revision(repo)
        if in_production():
            repo_git.checkout(merge_request.source_branch)
        else:
            repo_git.checkout("-t", f"origin/{merge_request.source_branch}")
        repo_git.checkout("-b", Branch().for_wip)
        try:
            repo_git.rebase(target_project.default_branch)
        except git.exc.GitCommandError:
            repo_git.rebase("--abort")
            logger.warning(
                "Rebase %s (aka theirs) with %s (aka ours) failed. "
                "Retry rebase with strategy option 'ours' to prevail what's "
                "in the project.",
                merge_request.source_branch,
                target_project.default_branch,
            )
            repo_git.rebase(
                target_project.default_branch,
                "--strategy=recursive",
                "--strategy-option=ours",
            )
            push_to_the_fork(repo, wait_pipeline_creation=False)
            logger.info(
                "Start with the rebased (with %s branch) commit with SHA %s (%s).",
                target_project.default_branch,
                repo_head.commit.hexsha,
                fork_project.commits.get(repo_head.commit.hexsha).web_url,
            )
        return True, old_revision
    else:
        repo_head = repo.head  # type: ignore
        repo_heads = repo.heads  # type: ignore
        # if there isn't a previous uprev proposal, the wip branch starts from
        # the default branch
        repo_heads[target_project.default_branch].checkout(
            force=True, b=Branch().for_wip
        )
        logger.info(
            "Start working from the commit with SHA %s (%s)",
            repo_head.commit.hexsha,
            target_project.commits.get(repo_head.commit.hexsha).web_url,
        )
        return False, ""


def __check_dependency_revision(repo: git.Repo) -> str:
    """
    From a clone from the default branch, read which is the dependency's
    revision in use in the project, to store it for later use.
    The main purpose is, then it is continuing a previous proposal, inject the
    old revision to build correctly the diff_url.
    :param repo: the object managing the git clone
    :return: the revision of the dependency in the default branch
    """
    logger.debug(
        "Dry run an uprev using the information in %s",
        get_target_project().gl_project.default_branch,
    )
    uprev_obj = __do_uprev(repo, dry_run=True)
    return uprev_obj.old_revision


def __do_uprev(
    repo: git.Repo,
    pipeline_id: int | None = None,
    revision: str | None = None,
    templates_commit: str | None = None,
    amend_previous_uprev: bool = False,
    dry_run: bool = False,
    inject_old_revision: str | None = None,
) -> UprevActionProcessor:
    """
    Given all the necessary data, proceed with the uprev core procedure.
    :param repo: the object managing the git clone
    :param pipeline_id: pipeline id from where the uprev candidate
           were selected
    :param revision: uprev candidate revision
    :param templates_commit: revision of the templates project
    :param amend_previous_uprev: if there is a previous uprev to continue
    :param dry_run: flag to avoid to write in the local clone
    :param inject_old_revision: original revision in default branch to replace
           the value when continue a previous attempt
    :return: Object representing and managing the uprev core procedure
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        if pipeline_id is None:
            if not dry_run:
                raise ValueError("Pipeline ID argument not initialized")
            pipeline_id = 0
        if revision is None:
            if not dry_run:
                raise ValueError("Revision argument not initialized")
            revision = ""
        if templates_commit is None:
            if not dry_run:
                raise ValueError("Templates commit argument not initialized")
            templates_commit = ""
        uprev_obj = uprev_mesa_in_virglrenderer(
            repo,
            pipeline_id,
            revision,
            templates_commit,
            amend=amend_previous_uprev,
            inject_old_revision=inject_old_revision,
            dry_run=dry_run,
        )
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        if revision is None:
            if not dry_run:
                raise ValueError("Revision argument not initialized")
            revision = ""
        uprev_obj = uprev_piglit_in_mesa(
            repo,
            revision,
            amend=amend_previous_uprev,
            inject_old_revision=inject_old_revision,
            dry_run=dry_run,
        )
    else:
        raise NotImplementedError(f"Not yet implemented the uprev of {uprev_pair.name}")
    Dependency().diff = uprev_obj.diff_url
    if not dry_run:
        logger.info(
            "Created commit with SHA %s. Changes in the dependency %s",
            repo.head.commit.hexsha,
            uprev_obj.diff_url,
        )
    return uprev_obj


def __get_branch(name: str) -> Optional[ProjectBranch]:
    """
    Get the branch gitlab object or return None hiding any exception
    :param name: string with branch name
    :return: The project branch object or None
    """
    try:
        fork_project = Fork().gl_project
        return fork_project.branches.get(name)
    except GitlabGetError:
        return None


def __wait_for_running_pipelines_in_branch(branch: ProjectBranch) -> None:
    """
    When a branch have pipelines running, wait until they finish. They could be
    part of a procedure that could fail if we intervene.
    :param branch: branch that could have pipelines running.
    :return:
    """
    fork_project = Fork().gl_project
    uprev_tmp_branch_name = Branch().for_wip
    while active_pipelines := [
        pipeline.id
        for pipeline in fork_project.pipelines.list(
            sha=branch.commit["id"], iterator=True
        )
        if pipeline.status == "running"
    ]:
        if len(active_pipelines) > 0:
            logger.warning(
                "Branch %s has %d pipeline(s) running. Waiting them to finish... (%s)",
                uprev_tmp_branch_name,
                len(active_pipelines),
                active_pipelines,
            )
            time.sleep(CHECKER_SLEEP_TIME)
    return None


def push_to_the_fork(
    repo: git.Repo, wait_pipeline_creation: bool = True
) -> Optional[ProjectPipeline]:
    """
    Push the commits in the local repository to the fork and wait until the
    last commit has a pipeline created (if not explicitly skipped).
    :param repo: local clone
    :param wait_pipeline_creation: boolean to skip the pipeline creation check
    :return: commit pipeline
    """
    remote = repo.remote("fork")
    logger.info("Push to the fork.")
    remote.push(force=True)
    logger.info("Repository head sha %s", repo.head.commit.hexsha)
    Working().project = Fork()
    if wait_pipeline_creation:
        return __wait_pipeline_creation(repo.head.commit.hexsha)
    return None


def __wait_pipeline_creation(
    commit_hash: str, source: str | None = None
) -> ProjectPipeline:
    """
    For a pushed commit to the fork, Peaceful wait for the pipeline creation.
    We can use the 'source' parameter to fine-tune the pipeline wanted.
    :param commit_hash: string
    :param source: string
    :return: pipeline
    """
    logger.info(
        "Waiting for pipeline to be created for %s %s",
        commit_hash,
        __get_commit(commit_hash).web_url,
    )
    t_0 = datetime.now()
    query = {"sha": commit_hash}
    if source:
        query["source"] = source
    while True:
        pipeline = __query_pipeline(query)
        if pipeline:
            return pipeline
        # when not found
        if (datetime.now() - t_0).total_seconds() >= GL_OBJ_CREATION_TIMEOUT:
            raise RuntimeWarning(
                f"Timeout reached while waiting for pipeline creation for commit {commit_hash}"
            )
        time.sleep(CHECKER_SLEEP_TIME)


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_commit(commit_hash: str) -> ProjectCommit:
    """
    Get a commit object from the working project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param commit_hash: identifier string of the commit1
    :return: object in gitlab representing a commit.
    """
    try:
        working_project = Working().gl_project
        if working_project is None:
            raise ValueError("Working project not initialized")
        return working_project.commits.get(commit_hash)
    except Exception as exception:
        logger.error(
            "Couldn't get the commit %s due to %s: %s",
            commit_hash,
            type(exception),
            exception,
        )
        raise exception


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
def __query_pipeline(query: dict[str, str]) -> Optional[ProjectPipeline]:
    """
    Get the first pipeline in a list generated by a query. When the list is
    empty, return None. But if there is any other exception, it uses tenacity's
    retry.
    :param query: dictionary as the query_parameters in GL api
    :return: the pipeline or None
    """
    try:
        project = Working().gl_project
        if project is None:
            raise ValueError("Working project not initialized")
        pipeline = next(
            iter(project.pipelines.list(query_parameters=query, iterator=True))
        )
        return project.pipelines.get(pipeline.id)
    except StopIteration:
        return None


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
def __get_pipeline(idn: int, project: Project | None = None) -> ProjectPipeline:
    if project is None:
        project = Working().gl_project
        if project is None:
            raise ValueError("Working project not initialized")
    return project.pipelines.get(idn)


def run_pipeline(pipeline: ProjectPipeline) -> dict[str, dict[str, deque[str]]]:
    """
    Trigger the manual jobs in the pipeline (waiting, if necessary, to the
    complete creation of the pipeline in the server) and wait until the
    pipeline finishes.
    Then collect the artifacts. Give it a second chance if any jobs still need
    to produce artifacts. And also, run a second time the ones that made
    failures artifacts.
    :param pipeline: git lab object
    :return: failures dict
    """
    logger.info(
        "Waiting for pipeline to be ready %s (%s)",
        pipeline.web_url,
        pipeline.status,
    )
    while pipeline.status not in ["manual", "running"]:
        time.sleep(CHECKER_SLEEP_TIME)
        pipeline = __get_pipeline(pipeline.id)
    logger.info(
        "Triggering jobs for pipeline %s (%s)",
        pipeline.web_url,
        pipeline.status,
    )
    __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    return __post_process_pipeline(pipeline)


def __pipeline_trigger_jobs(
    pipeline: ProjectPipeline,
    classification: JobStageGroup,
) -> int:
    """
    Check if there are jobs in the pipeline that requires to be triggered.
    Depending on the project, the build jobs are grouped in some stages.
    :param pipeline: ProjectPipeline
    :param classification: information about which jobs should be triggered
    :return: number of triggered jobs
    """
    triggered = defaultdict(list)

    def __trigger_job(job: ProjectJob) -> None:
        if __pipeline_trigger_job_condition(job, classification):
            pjob = __get_job(job.id, lazy=True)
            pjob.play()
            logger.debug("Triggered %s (%s) job", job.name, job.stage)
            triggered[job.stage].append(job.name)

    with ThreadPoolExecutor(max_workers=16) as executor:
        t_0 = datetime.now()
        triggering = [
            executor.submit(__trigger_job, __get_job(job.id))
            for job in pipeline.jobs.list(
                iterator=True, include_retried=False, scope="manual"
            )
        ]
        wait_threads_pool(triggering)
        logger.debug("Trigger threaded loop took %s", datetime.now() - t_0)

    if triggered:
        logger.info("Trigger jobs iteration: %s", dict(triggered))
    return len(triggered.items())


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
def __get_job(idn: int, lazy: bool = False) -> ProjectJob:
    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    return working_project.jobs.get(idn, lazy=lazy)


def __pipeline_trigger_job_condition(job: ProjectJob, condition: JobStageGroup) -> bool:
    """
    Determines whether a pipeline job should be triggered based on certain
    conditions.
    :param job: The pipeline job to evaluate.
    :param condition: condition based on the job classification.
    :return: Boolean indicating whether the job should be triggered.
    """
    match condition:
        case JobStageGroup.build:
            meets_condition = __is_build_job
        case JobStageGroup.test:
            meets_condition = __is_test_job
        case JobStageGroup.other:
            meets_condition = __is_other_job
        case _:
            return False
    if job.status == "manual" and meets_condition(job):
        return True
    return False


def __post_process_pipeline(
    pipeline: ProjectPipeline,
) -> dict[str, dict[str, deque[str]]]:
    """
    Check if there are results in the pipeline jobs. When some job has finished
    without artifacts, give it a second chance to make them.
    To detect flakes in the tests, the failed jobs are retried to see if they
    produce the same outcome.
    :param pipeline: pipeline to post process
    :return: dictionary with the failures found
    """
    failures, artifacts, wo_artifacts, stuck = collate_results(pipeline)
    if stuck:
        logger.warning(
            "Some jobs failed because not DUTs available (stuck): %s",
            ", ".join([job.name for job in stuck]),
        )
    if wo_artifacts:
        logger.warning(
            "Some jobs failed without producing artifacts: %s",
            ", ".join([job.name for job in wo_artifacts]),
        )
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, wo_artifacts
        )
    if artifacts:
        logger.info("Retry failed jobs looking for flakes.")
        exclude = [job.name for job in wo_artifacts + stuck]
        _, failures, _ = __retry_flake_candidates(
            pipeline, failures, artifacts, exclude
        )

    return failures


def __wait_pipeline_start(
    pipeline: ProjectPipeline,
) -> Tuple[ProjectPipeline, dict[str, str]]:
    """
    Once a manual jobs are triggered in a pipeline, there is a (usually short
    but non-negligible) time.
    It return a refreshed pipeline object, dictionary of jobs with they status.
    :param pipeline: ProjectPipeline
    :return: ProjectPipeline, dict
    """
    logger.info(
        "Wait for pipeline %s to start (status=%s)",
        pipeline.web_url,
        pipeline.status,
    )
    jobs_stata: dict[str, str] = {}
    while True:
        _, pipeline = __refresh_pipeline_status(pipeline)
        jobs_stata, _ = __refresh_pipeline_job_status(pipeline, jobs_stata)
        n_triggered = __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
        if pipeline.status in ["running", "failed"]:
            logger.info(
                "Pipeline %s in %s status",
                pipeline.web_url,
                pipeline.status,
            )
            return pipeline, jobs_stata
        if n_triggered == 0 and pipeline.status == "manual":
            logger.info(
                "Pipeline %s in %s status, without build jobs to waiting for a trigger.",
                pipeline.web_url,
                pipeline.status,
            )
            return pipeline, jobs_stata
        time.sleep(CHECKER_SLEEP_TIME)


def __wait_pipeline_finished(
    pipeline: ProjectPipeline, jobs_stata: dict[str, str]
) -> ProjectPipeline:
    """
    Periodically check if the pipeline has finished.
    It received a pipeline object and a dictionary with the last status of the
    jobs in the pipeline. Job changing status is logged, and it returns a
    refreshed pipeline object.
    :param pipeline: ProjectPipeline
    :param jobs_stata
    :return: ProjectPipeline
    """
    if pipeline.status in ["failed"]:
        return pipeline
    logger.info(
        "Wait for pipeline %s to finish (status=%s)",
        pipeline.web_url,
        pipeline.status,
    )
    stata: dict[str, int] = {}
    minutes_threshold = 10
    loop_time = timedelta(seconds=PIPELINE_CHECKER_SLEEP_TIME)
    checker_step = timedelta(seconds=PIPELINE_CHECKER_SLEEP_TIME)
    loop_time_history: list[int] = []
    dont_retry: list[int] = []
    while True:
        t0 = datetime.now()
        status_changed, pipeline = __refresh_pipeline_status(pipeline)
        jobs_stata, stata = __refresh_pipeline_job_status(pipeline, jobs_stata, stata)
        __pipeline_trigger_jobs(pipeline, JobStageGroup.build)
        __pipeline_trigger_jobs(pipeline, JobStageGroup.test)
        dont_retry = __review_failed_jobs(pipeline, dont_retry)
        if status_changed and pipeline.status not in [
            "created",
            "waiting_for_resource",
            "preparing",
            "pending",
            "running",
        ]:
            logger.info(
                "Pipeline %s finished (status=%s)",
                pipeline.web_url,
                pipeline.status,
            )
            return pipeline
        time_in_current_status = datetime.now(timezone.utc) - datetime.strptime(
            f"{pipeline.updated_at[:19]}Z", "%Y-%m-%dT%H:%M:%S%z"
        )
        if (time_in_current_status.total_seconds() // 60) > minutes_threshold:
            minutes_threshold += 10
            logger.info(
                "Pipeline in '%s' for %s and continue.",
                pipeline.status,
                time_in_current_status,
            )
        loop_time_history, loop_time, sleep_time_left = __loop_time_monitor(
            datetime.now() - t0,
            loop_time_history,
            loop_time,
            checker_step,
        )
        seconds_to_sleep = sleep_time_left.total_seconds()
        if seconds_to_sleep > 0:
            logger.debug(
                "Wait pipeline to finish loop going to sleep for %s seconds",
                sleep_time_left,
            )
            time.sleep(seconds_to_sleep)


def __loop_time_monitor(
    time_spend: timedelta,
    time_spend_history: list[int],
    expected_loop_time: timedelta,
    step: timedelta,
) -> Tuple[list[int], timedelta, timedelta]:
    """
    Given the current time spend in the monitored loop, as well as the time spend in previous loops,
    check if the expectation on the loop time needs to be relaxed or tight.
    :param time_spend: time spend in the current loop
    :param time_spend_history: record of the previous loops
    :param expected_loop_time: expected time to be adjusted
    :param step: increment/decrement of the expected loop time
    :return: newer historical record of time spend, new loop time to expect, proposed sleep
    """
    time_spend_history.append(time_spend.seconds)
    time_spend_history = time_spend_history[-10:]  # cyclic buffer
    previous_loops_average = timedelta(seconds=mean(time_spend_history))
    # overrun directly increase the time
    if time_spend > expected_loop_time:
        expected_loop_time += step
        logger.warning(
            "Pipeline check loop took %s, adjusting loop time to %s.",
            time_spend,
            expected_loop_time,
        )
    # underrun requires last N loops
    elif previous_loops_average <= expected_loop_time - step:
        expected_loop_time -= step
        logger.info(
            "Last 10 iterations in pipeline check loop took %s, adjusting wait time to %s",
            previous_loops_average,
            expected_loop_time,
        )
    logger.debug("Wait pipeline to finish loop took %s", time_spend)
    sleep_time_left = expected_loop_time - time_spend
    return time_spend_history, expected_loop_time, sleep_time_left


def __refresh_pipeline_status(
    pipeline: ProjectPipeline,
) -> Tuple[bool, ProjectPipeline]:
    """
    Check if the pipeline status has changed since the last object refresh.
    :param pipeline: ProjectPipeline
    :return: bool, ProjectPipeline
    """
    previous_pipeline_status = pipeline.status
    pipeline = __get_pipeline(pipeline.id)
    if previous_pipeline_status != pipeline.status:
        logger.debug(
            "Pipeline %d status change: %s -> %s",
            pipeline.id,
            previous_pipeline_status,
            pipeline.status,
        )
        return True, pipeline
    return False, pipeline


def __refresh_pipeline_job_status(
    pipeline: ProjectPipeline,
    jobs_status: dict[str, str],
    stata: dict[str, int] | None = None,
) -> Tuple[dict[str, str], dict[str, int]]:
    """
    It checks if there were any change in the state of the jobs of the
    pipeline.
    :param pipeline: ProjectPipeline
    :param jobs_status: dict
    :param stata: dict
    :return: (dict, dict)
    """
    new_stata: dict[str, int] = defaultdict(int)
    log_states = ["manual", "pending"]
    log_jobs: dict[str, dict[str, list[str]]] = defaultdict(lambda: defaultdict(list))

    def __review_job(job: ProjectJob) -> None:
        new_stata[job.status] += 1
        if not __is_build_job(job) and not __is_test_job(job):
            return
        if job.status in log_states:
            log_jobs[job.status][job.stage].append(job.name)
        if job.name not in jobs_status.keys():
            logger.debug(
                "Pipeline job '%s' ('%s') in '%s' status.",
                job.name,
                job.stage,
                job.status,
            )
            jobs_status[job.name] = job.status
        elif job.status != jobs_status[job.name]:
            logger.debug(
                "Pipeline job '%s' ('%s') status change from '%s' to '%s'.",
                job.name,
                job.stage,
                jobs_status[job.name],
                job.status,
            )
            jobs_status[job.name] = job.status

    with ThreadPoolExecutor(max_workers=32) as executor:
        t_0 = datetime.now()
        bar = [
            executor.submit(__review_job, __get_job(job.id))
            for job in pipeline.jobs.list(
                iterator=True, include_retried=False, sort_by="stage"
            )
        ]
        wait_threads_pool(bar)
        logger.debug("Refresh threaded loop took %s", datetime.now() - t_0)

    new_stata = dict(OrderedDict(sorted(new_stata.items())))
    if stata != new_stata:
        logger.info("Jobs stata summary %s", new_stata)
        for job_status, jobs_dict in log_jobs.items():
            if (
                not isinstance(stata, dict)
                or job_status not in stata
                or job_status not in new_stata
                or stata[job_status] != new_stata[job_status]
            ):
                logger.warning("Jobs in %s: %s", job_status, dict(jobs_dict))
    return jobs_status, dict(new_stata)


def __review_failed_jobs(pipeline: ProjectPipeline, dont_retry: list[int]) -> list[int]:
    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    all_jobs: dict[str, list[int]] = defaultdict(list)
    failed_jobs: list[str] = []
    for job in pipeline.jobs.list(iterator=True, include_retried=True):
        job = working_project.jobs.get(job.id)
        all_jobs[job.name].append(job.id)
        if job.status == "failed":
            failed_jobs.append(job.name)
    for job_name in failed_jobs:
        if len(all_jobs[job_name]) < FAILED_JOB_RETRIES:
            # Below the number of retries, get the last attempt, and retry
            job_ids = all_jobs[job_name]
            job_ids.sort()
            job_id = job_ids[-1]
            if job_id in dont_retry:
                continue
            try:
                pjob = __get_job(job_id, lazy=True)
                pjob.retry()
                logger.info("%s failed, retry...", job_name)
            except GitlabJobRetryError as exc:
                logger.warning(
                    "%s failed (%d), and exception catch in retry: %s",
                    job_name,
                    job_id,
                    exc.response_body,
                )
                dont_retry.append(job_id)
    return dont_retry


def __retry_jobs_without_artifacts(
    pipeline: ProjectPipeline,
    artifacts: dict[str, bytes],
    jobs_without_artifacts: list[ProjectJob],
) -> Tuple[ProjectPipeline, dict[str, bytes]]:
    """
    if there are jobs without artifacts, lets give them another try
    :return:
    """
    logger.debug("Some jobs didn't produce artifacts")
    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    for job in jobs_without_artifacts:
        logger.info("Retry '%s' for artifacts build.", job.name)
        pjob = working_project.jobs.get(job.id, lazy=True)
        pjob.retry()
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    # collect results again, but:
    # - remembering artefacts already downloaded to avoid overwriting
    _, artifacts, jobs_without_artifacts, _ = collate_results(
        pipeline, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        job_names = [job.name for job in jobs_without_artifacts]
        join_job_names = ", ".join(job_names)
        __uprev_failed_issue(
            pipeline,
            f"These jobs failed to produce artifacts for a second time: {join_job_names}",
        )
        sys.exit(0)
    return pipeline, artifacts


def __retry_flake_candidates(
    pipeline: ProjectPipeline,
    failures: dict[str, dict[str, deque[str]]],
    artifacts: dict[str, bytes],
    exclude: list[str],
) -> Tuple[ProjectPipeline, dict[str, dict[str, deque[str]]], dict[str, bytes]]:
    """
    Do one retry in the jobs that may have failed to
    :param pipeline: ProjectPipeline
    :param failures: dictionary with already know failures per job
    :param artifacts: dictionary of the artifacts per job
    :param exclude: jobs to exclude in the retry
    :return: ProjectPipeline
    """
    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    for job in pipeline.jobs.list(iterator=True):
        if job.status == "failed" and job.name not in exclude:
            logger.info("Retry '%s' to discard flakes", job.name)
            pjob = working_project.jobs.get(job.id, lazy=True)
            pjob.retry()
    pipeline, jobs_stata = __wait_pipeline_start(pipeline)
    pipeline = __wait_pipeline_finished(pipeline, jobs_stata)
    # collect results again, but:
    # - remembering failures to collect together new results
    # - remembering artefacts already downloaded to avoid duplication
    _, _, jobs_without_artifacts, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts
    )
    if len(jobs_without_artifacts) != 0:
        pipeline, artifacts = __retry_jobs_without_artifacts(
            pipeline, artifacts, jobs_without_artifacts
        )
    failures, artifacts, _, _ = collate_results(
        pipeline, failures=failures, artifacts=artifacts, recollect=True
    )
    return pipeline, failures, artifacts


def __uprev_failed_issue(
    pipeline: ProjectPipeline,
    reason: str,
) -> None:
    """
    When the uprev produces jobs that doesn't produce artifacts, it means there
    is something deeper about the uprev that cannot be solved with an
    expectations update. Further information has to be provided to the
    developers to know about the situation.
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        ci_project_id = os.getenv("CI_PROJECT_ID")
        if ci_project_id is None:
            description = [f"This information comes from the ci-uprev job {job_id}.\n"]
        else:
            uprev_project = gl.projects.get(ci_project_id)
            uprev_job = uprev_project.jobs.get(job_id)
            description = [
                f"This information comes from the execution of ci-uprev in job [{uprev_job.id}]({uprev_job.web_url}).\n\n"
            ]
    else:
        description = ["This information comes from a local execution of ci-uprev.\n\n"]
    description.append(f"Related pipeline: [{pipeline.id}]({pipeline.web_url}).\n")
    description.append(f"ci-uprev reported:\n\n\t{reason}\n")
    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    uprev_commit = working_project.commits.get(pipeline.sha)
    start_commt = working_project.commits.get(uprev_commit.parent_ids[0])
    dep_project = Dependency().gl_project
    dep_commit = dep_project.commits.get(uprev_commit.title.split()[-1])
    target_project = get_target_project().gl_project
    description.append(
        f"This happened while doing an update revision of {dep_project.name} "
        f"[revision {dep_commit.short_id}]({dep_commit.web_url}) in "
        f"{target_project.name} [revision "
        f"{start_commt.short_id}]({start_commt.web_url}) with the uprev "
        f"[commit {uprev_commit.short_id}]({uprev_commit.web_url})."
    )
    __publish_uprev_issue("".join(description))


def __publish_uprev_issue(description: str) -> None:
    """
    When is necessary to publish some information in an issue in the target
    project, this method does that. But it only creates a new issue if there
    isn't one already opened. If there is already an issue, it appends a note.
    :param description: body text of the issue
    """
    issue: ProjectIssue | None = __search_existing_uprev_issue()
    issue_title = Title().for_issue
    note: ProjectIssueNote | None = None
    target_project = get_target_project().gl_project
    if issue is None:
        labels = __get_uprev_labels()
        t_0 = datetime.now()
        issue_rest_obj = target_project.issues.create(
            {"title": issue_title, "description": description, "labels": labels}
        )
        logger.info(
            "Created an issue with id %d, wait until the issue is ready to be used.",
            issue_rest_obj.iid,
        )
        while True:
            issue = __get_issue(issue_rest_obj.iid)
            if issue is not None:
                break
            accumulated_wait_time = (datetime.now() - t_0).total_seconds()
            if accumulated_wait_time >= GL_OBJ_CREATION_TIMEOUT:
                raise RuntimeWarning(
                    f"Timeout reached while waiting for issue creation for id {issue_rest_obj.iid}"
                )
            time.sleep(CHECKER_SLEEP_TIME)
        logger.info("Created an issue with the runtime warning %s", issue.web_url)
        note = None
    else:
        t_0 = datetime.now()
        note_rest_obj = issue.notes.create({"body": description})
        logger.info("Created a note %d in the issue %d", note_rest_obj.id, issue.iid)
        while True:
            note = __get_issue_note(issue, note_rest_obj.id)
            if note is not None:
                break
            accumulated_wait_time = (datetime.now() - t_0).total_seconds()
            if accumulated_wait_time >= GL_OBJ_CREATION_TIMEOUT:
                raise RuntimeWarning(
                    f"Timeout reached while waiting for note creation for id {note_rest_obj.id} in issue {issue.iid}"
                )
            time.sleep(CHECKER_SLEEP_TIME)
        note_web_url = f"{issue.web_url}#note_{note.id}"
        logger.info(
            "Append a note to an already existing issue with the runtime warning %s",
            note_web_url,
        )
    __link_issue_with_merge_request(issue, note)


def __search_existing_uprev_issue() -> Optional[ProjectIssue]:
    """
    Search in the target project if there is already an issue open about this
    uprev.
    :return: if exists, the issue, else None
    """
    target_project = get_target_project().gl_project
    query = {
        "state": "opened",
        "search": Title().for_issue,
        "in": "title",
        "order_by": "created_at",
    }
    try:
        issue = next(
            iter(target_project.issues.list(query_parameters=query, iterator=True))
        )
        return target_project.issues.get(issue.iid)
    except StopIteration:
        return None


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_issue(issue_iid: int) -> Optional[ProjectIssue]:
    """
    Get a issue object from the target project. If there is an exception, log
    it and reraise. It has also a decorator to use tenacity to retry.
    :param issue_iid: internal identifier number of the issue
    :return: object in gitlab representing an issue.
    """
    try:
        target_project = get_target_project().gl_project
        return target_project.issues.get(issue_iid)
    except GitlabGetError as exception:
        if exception.response_code == 404:
            return None
        logger.debug(
            "Couldn't get the issue %d due to %d: %s",
            issue_iid,
            exception.response_code,
            exception.response_body,
        )
        raise exception
    except Exception as exception:
        logger.debug(
            "Couldn't get the issue %d due to %s: %s",
            issue_iid,
            type(exception),
            exception,
        )
        raise exception


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_issue_note(issue: ProjectIssue, note_id: int) -> Optional[ProjectIssueNote]:
    return issue.notes.get(note_id)


def __get_uprev_labels() -> list[str]:
    """
    Simple method where each target project can set up the labels issues or
    merge requests should have tagged with.
    :return: list of tags
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return ["ci"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        return []  # this is done by labelmaker
    else:
        return []


def __link_issue_with_merge_request(
    issue: ProjectIssue, note: ProjectIssueNote | None = None
) -> None:
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)
    if isinstance(merge_request, ProjectMergeRequest):
        description = ["It has been not possible to propose a newer uprev."]
        if not isinstance(note, ProjectIssueNote):
            description.append(f"Further information on the issue {issue.web_url}")
        else:
            description.append(f"Further information on the issue note {note.web_url}")
        merge_request.notes.create({"body": "".join(description)})
    else:
        logger.debug(
            "There isn't a merge request in progress to report the creation of the issue"
        )


def collate_results(
    pipeline: ProjectPipeline,
    failures: dict[str, dict[str, deque[str]]] | None = None,
    artifacts: dict[str, bytes] | None = None,
    recollect: bool = False,
) -> Tuple[
    dict[str, dict[str, deque[str]]],
    dict[str, bytes],
    list[ProjectJob],
    list[ProjectJob],
]:
    """
    Find and collect the artifacts of the jobs. Classify them in a dictionary
    or, if there isn't, collect those jobs in a list.
    :param pipeline: ProjectPipeline
    :param failures: dict
    :param artifacts: dict
    :param recollect: bool
    :return: failures, artifacts, wo_artifacts, stuck
    """
    logger.info("Looking for failed jobs in pipeline %s", pipeline.web_url)
    if artifacts is None:
        artifacts = dict()
    if failures is None:
        # failures dict has as keys the tripplet (test_suite, backend, api)
        #  as value it has a nested dict where the key is the test_name and
        #  the values are deque of ens states of the tests in different reties.
        failures = defaultdict(defaultdictdeque)
    else:
        failures = deepcopy(failures)  # propagate will be decided by return
    wo_artifacts: list[ProjectJob] = []
    stuck: list[ProjectJob] = []

    working_project = Working().gl_project
    if working_project is None:
        raise ValueError("Working project not initialized")
    build_jobs_failed = []
    for job in pipeline.jobs.list(iterator=True):
        job = working_project.jobs.get(job.id)

        if job.status != "failed":
            continue

        if __is_build_job(job):
            build_jobs_failed.append(f"{job.name} ({job.stage})")
            logger.critical(
                "Something very bad happened: a build or sanity test job failed! ('%s' job in '%s' stage)",
                job.name,
                job.stage,
            )
            continue
        elif not __is_test_job(job):
            logger.warning(
                "Something wrong with '%s' (in %s stage), but it's not affecting the uprev",
                job.name,
                job.stage,
            )
            continue

        job_classification_key = __unshard_job_name(job.name)

        if __download_artifacts(job, artifacts):
            results = artifacts[job.name]
        else:
            job_trace = job.trace(iterator=True)
            if job_trace is not None:
                stuck.append(job)
            else:
                wo_artifacts.append(job)
            continue

        __collate_failures(results, failures[job_classification_key], recollect)

    if build_jobs_failed:
        join_build_jobs_failed = ", ".join(build_jobs_failed)
        __uprev_failed_issue(
            pipeline,
            f"Something wrong with the build jobs: {join_build_jobs_failed}",
        )
        sys.exit(0)

    if recollect:
        __implicit_results_become_explicit(failures)

    return failures, artifacts, wo_artifacts, stuck


def __is_build_job(job: ProjectJob) -> bool:
    """
    The jobs can be classified first in two main groups: jobs that provides
    build things to other jobs, and jobs that perform tests themselves.
    This method reports if the give job is classified as a build job for the
    uprev purposes.
    :param job: ProjectJob
    :return: bool
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ["build", "sanity test"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        # not all the build stages, but the ones we have interest
        return job.stage in [
            "sanity",
            "container",
            # "git-archive",
            "build-for-tests",
            # "build-only",
            "code-validation",
            # "deploy",
        ]


def __is_test_job(job: ProjectJob) -> bool:
    """
    The jobs can be classified first in two main groups: jobs that provides
    build things to other jobs, and jobs that perform tests themselves.
    This method reports if the give job is classified as a test job for the
    uprev purposes.
    :param job: ProjectJob
    :return: bool
    """
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        return job.stage in ["test"]
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        # Some jobs can be filtered by name
        exclude_by_name = re.match(
            r".*(-vk|vkcts|lavapipe|traces|fossils|skqp|deqp|angle|nine).*", job.name
        )
        contain_piglit = re.match(r".*(piglit).*", job.name)
        if exclude_by_name and not contain_piglit:
            return False
        if re.match(r"(r300-).*", job.name) and job.stage == "amd-postmerge":
            return True
        if re.match(r"(panfrost-g52-).*", job.name) and job.stage == "arm-postmerge":
            return True
        if re.match(r"(a306-).*", job.name) and job.stage == "freedreno-postmerge":
            return True
        if re.match(r"(a530-).*", job.name) and job.stage == "freedreno-postmerge":
            return True
        if (
            re.match(r"(a618-piglit-full).*", job.name)
            and job.stage == "freedreno-postmerge"
        ):
            return True
        # not all the testing stages, but the ones we have interest
        return job.stage in [
            "amd",
            # "amd-postmerge",
            "intel",
            # "intel-postmerge",
            "nouveau",
            # "nouveau-postmerge",
            "arm",
            # "arm-postmerge",
            "broadcom",
            # "broadcom-postmerge",
            "freedreno",
            # "freedreno-postmerge",
            "etnaviv",
            # "etnaviv-postmerge",
            "software-renderer",
            # "software-renderer-postmerge",
            "layered-backends",
            # "layered-backends-postmerge",
            # "performance",
        ]


def __is_other_job(job: ProjectJob) -> bool:
    return not __is_build_job(job) and not __is_test_job(job)


def __download_artifacts(job: ProjectJob, artifacts: dict[str, bytes]) -> bool:
    """
    For a given job, find if it has left artifacts (there could be more than
    one location to look at) to be downloaded. If so, collect it to be
    processed next. On the contrary, report returning false.
    :return:
    """
    try:
        if job.name not in artifacts:
            tmp = TemporaryDirectory()
            artifacts_zip_name = f"{tmp.name}/artifacts.zip"
            with open(artifacts_zip_name, "wb") as f:
                job.artifacts(streamed=True, action=f.write)
            logger.info(
                "Downloaded artifacts of %s at %s",
                job.name,
                artifacts_zip_name,
            )
            artifacts_zip = ZipFile(artifacts_zip_name)
            artifact = b""
            artifact_found = False
            for element in artifacts_zip.filelist:
                if element.filename.endswith("failures.csv"):
                    logger.info("Artifact found: %s", element.filename)
                    # Concatenate the content for all the failures.csv files
                    artifact += artifacts_zip.read(element.filename)
                    artifact_found = True
            if not artifact_found:
                raise FileNotFoundError("No artifacts found")
            artifacts[job.name] = artifact
        return True
    except Exception as exception:
        logger.error(
            "An exception (%s) occurred when downloading results for '%s' at %s: %s!",
            type(exception),
            job.name,
            job.web_url,
            exception,
        )
        return False


def __collate_failures(
    results: bytes, job_failures: dict[str, deque[str]], recollect: bool
) -> None:
    """
    Process the lines in the results downloaded to insert the data to the
    failures structure of a given job.
    :param results: content of the artifact downloaded
    :param job_failures: key-value of the test and the results it has had
    :param recollect: if this happened after a failed jobs retry
    :return:
    """
    for line in results.decode("utf-8").split("\n"):
        if not line.strip():
            continue
        test_name, result = line.split(",")
        # detect flakiness: collect the current execution results
        job_failures[test_name].append(result)
        if recollect and len(job_failures[test_name]) == 1:
            # As we only collect when fail is reported or unexpected pass,
            #  complete the information
            failures_item = job_failures[test_name]
            if result == "UnexpectedPass":
                failures_item.appendleft("ExpectedFail")
            else:
                failures_item.appendleft("Pass")


def __implicit_results_become_explicit(
    failures: dict[str, dict[str, deque[str]]]
) -> None:
    """
    review and complete the list of pairs with the implicit pass
    :param failures: dictionary with the results already collected
    :return: dictionary with the implicit results made explicit
    """
    for test_dict in failures.values():
        for results_deque in test_dict.values():
            if len(results_deque) == 1:
                if results_deque[0] == "UnexpectedPass":
                    results_deque.append("ExpectedFail")
                else:
                    results_deque.append("Pass")


def __unshard_job_name(name: str) -> str:
    """
    Remove the suffix on the sharded jobs to have a single name
    for all of them.
    :param name: string
    :return: original name before sharding
    """
    # if job is sharded, it has a " n/m" at the end, remove it.
    if search := re.match(r"(.*) [0-9]+/[0-9]+$", name):
        name = search.group(1)
    # remove the '-full' suffix
    if search := re.match(r"(.*)[-_]full$", name):
        name = search.group(1)
    # when after "-full", we have the arch tag
    elif search := re.match(r"(.*)[-_]full(:.*)$", name):
        name = f"{search.group(1)}{search.group(2)}"
    return name


def update_branch(
    repo: git.Repo,
    failures: dict[str, dict[str, deque[str]]],
    do_commit: bool = True,
) -> None:
    """
    Using the local clone of the repository, review the failures found
    (if there are any) to update the expectations (if required).
    :param repo: working repository
    :param failures: dictionary of the failures to update
    :param do_commit: flag to define if the clone should be modified
    :return:
    """
    os.chdir(repo.working_dir)
    try:
        index = repo.index  # This is expensive, reuse the index object
        update_expectations = False
        for job_key, test_dict in failures.items():
            expectations = __get_expectations_path_and_file(job_key)
            if expectations is None:
                continue
            fails, flakes, skips = __get_expectations_contents(*expectations)
            existing_fails = __fail_tests_and_their_causes(fails["content"])

            fixed_tests, update_expectations = __include_new_fails_and_flakes(
                job_key,
                test_dict,
                flakes,
                fails,
                skips,
                existing_fails,
            )

            __remove_fixed_tests(fixed_tests, fails["content"])

            for file_name, content in [
                (flakes["name"], flakes["content"]),
                (fails["name"], fails["content"]),
                (skips["name"], skips["content"]),
            ]:
                __patch_content(index, file_name, content, do_commit)

        if do_commit:
            if update_expectations:
                repo.git.commit("--amend", "--no-edit")
                logger.info("Amended commit with new SHA %s\n", repo.head.commit.hexsha)
            __rename_active_branch(repo, Branch().for_merge_request)
    finally:
        os.chdir("..")


def __rename_active_branch(repo: git.Repo, new_name: str) -> None:
    """
    To complete the rename action, it is necessary to do it in two steps
    because the gitpython operation doesn't remove the old one in the remote.
    The new branch is only local now. The step to push it comes next by call
    push_to_merge_request().
    :param repo: working repository
    :param new_name: destination name for the temporary branch
    :return:
    """
    repo.active_branch.rename(new_name, force=True)
    try:
        # This branch is exceedingly rare in the remote, but it may happen if
        #  there is an interrupted execution of `ci-uprev` that pushes the
        #  branch and breaks before continuing the process.
        repo.remotes.fork.push(refspec=f":{Branch().for_wip}")
        logger.debug(
            "Remove the %s in the remote fork project.",
            Branch().for_wip,
        )
    except git.exc.GitCommandError:
        logger.debug(
            "The remote fork project doesn't have an orphan %s", Branch().for_wip
        )


def __get_expectations_path_and_file(job_key: str) -> Optional[Tuple[str, str]]:
    """
    Find in the structure where are located the expectation files for a given
    job. So it returns two strings. The first is with the path within the
    project. The second is with the prefix of the files that store the fails,
    flakes, and skips.
    :param job_key: string
    :return: a pair of strings
    """
    try:
        uprev_pair = UpdateRevisionPair().enum
        if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
            expectations_paths = virglrenderer_expectations_paths
        elif uprev_pair == UpdateRevision.piglit_in_mesa:
            expectations_paths = mesa_expectations_paths
        else:
            target_project = Target().gl_project
            raise NotImplementedError(
                f"Not yet implemented the expectations update for {target_project.name}"
            )
        path = expectations_paths[job_key]["path"]
        files = expectations_paths[job_key]["files"]
        if not path or not files:
            raise KeyError
        logger.debug("For %r the path is %r and files %r", job_key, path, files)
        return path, files
    except KeyError:
        logger.warning("The tool is not yet managing expectations for %s", job_key)
        return None


def __get_expectations_contents(
    path: str, prefix: str
) -> Tuple[dict[str, Any], dict[str, Any], dict[str, Any]]:
    """
    Get the contents of the files that could be necessary to update the
    expectations.
    :param path: string
    :param prefix: string
    :return: three dicts with the name and content of the files
    """
    fails_file_name = f"{path}/{prefix}-fails.txt"
    fails = {"name": fails_file_name, "content": __get_file_content(fails_file_name)}
    flakes_file_name = f"{path}/{prefix}-flakes.txt"
    flakes = {"name": flakes_file_name, "content": __get_file_content(flakes_file_name)}
    skips_file_name = f"{path}/{prefix}-skips.txt"
    skips = {"name": skips_file_name, "content": __get_file_content(skips_file_name)}
    return fails, flakes, skips


def __fail_tests_and_their_causes(
    fails_contents: list[str],
) -> dict[str, dict[str, Any]]:
    """
    From the list of lines in the fails file, convert it to a dictionary where
    the key is the test name, and the value is the test results and the line.
    We can later check if the test has failed for the same reason or another.
    :param fails_contents:
    :return:
    """
    dct = {}
    for i, line in enumerate(fails_contents, start=1):
        if line.startswith("#") or line.count(",") == 0:
            continue  # ignore these lines
        test_name, test_result = line.strip().rsplit(",", 1)
        dct[test_name] = {"result": test_result, "line": i}
    return dct


def __include_new_fails_and_flakes(
    job_key: str,
    tests: dict[str, deque[str]],
    flakes: dict[str, Any],
    fails: dict[str, Any],
    skips: dict[str, Any],
    existing_fails: dict[str, dict[str, str]],
) -> Tuple[list[str], bool]:
    """
    Prepare the content for the fails and flake files with the newer
    information from the uprev pipeline execution.
    :rtype: object
    :param job_key: identifier
    :param tests: list
    :param flakes: file name and content
    :param fails: file name and content
    :param skips: file name and content
    :param existing_fails: dict with the known fails and their result
    :return: list of test that are fix with the uprev, flag about if it has
             made an expectations update
    """
    expectations_change = False
    fixed_tests = []
    for test_name, test_results in tests.items():
        # remove duplicated elements
        test_results_unique = set(test_results)
        assert (
            len(test_results_unique) > 0
        ), "Failed, tests should have at least one result"
        if len(test_results_unique) != 1:
            logger.debug(
                "Flaky test detected in %s test_name=%s with results %s",
                job_key,
                test_name,
                list(test_results),
            )
            flakes["content"] += [f"{test_name}\n"]
            expectations_change = True
        elif test_results[0] in [
            "UnexpectedPass",
            "UnexpectedImprovement(Pass)",
            "UnexpectedImprovement(Skip)",
            "UnexpectedImprovement(Warn)",
        ]:
            logger.debug(
                "%s test detected in %s test_name=%s",
                test_results[0],
                job_key,
                test_name,
            )
            fixed_tests += [test_name]
            expectations_change = True
        else:
            if test_results[0].startswith("UnexpectedImprovement"):
                # When there is an unexpected improvement,
                # use the value in the parenthesis to proceed.
                if match := re.match(r"UnexpectedImprovement\((.*)\)", test_results[0]):
                    test_results = deque([match.group(1)] * len(test_results))
            if test_results[0] in ["Timeout"]:
                file_name = skips["name"]
                content = skips["content"]
            else:
                file_name = fails["name"]
                content = fails["content"]
            if test_name in existing_fails.keys():
                previous_test_result = existing_fails[test_name]["result"]
                line = existing_fails[test_name]["line"]
                if test_results[0] == previous_test_result:
                    logger.debug(
                        "Failed test detected in %s test_name=%s with results '%s' "
                        "was already in %s:%d with the same cause.",
                        job_key,
                        test_name,
                        test_results[0],
                        file_name,
                        line,
                    )
                    # This shouldn't happen, but log it just in case.
                else:
                    logger.debug(
                        "Failed test detected in %s test_name=%s with results '%s' "
                        "was already in %s:%d with a different cause (was '%s').",
                        job_key,
                        test_name,
                        test_results[0],
                        file_name,
                        line,
                        previous_test_result,
                    )
                    if test_name not in content:
                        content.append(f"{test_name}\n")
                    fixed_tests.append(test_name)
                    expectations_change = True
            else:
                logger.debug(
                    "Failed test detected in %s test_name=%s with results %s",
                    job_key,
                    test_name,
                    test_results[0],
                )
                content += [f"{test_name},{test_results[0]}\n"]
                expectations_change = True
    return fixed_tests, expectations_change


def __remove_fixed_tests(fixed_tests: list[str], fails_content: list[str]) -> None:
    """
    Review the list of tests that are fixed with the uprev to remove them from
    the list of tests that it was known they fail.
    :param fixed_tests:
    :param fails_content:
    :return:
    """
    for test_name in fixed_tests:
        found = False
        if f"{test_name},Fail\n" in fails_content:
            fails_content.remove(f"{test_name},Fail\n")
            found = True
        if f"{test_name},Crash\n" in fails_content:
            fails_content.remove(f"{test_name},Crash\n")
            found = True
        if found:
            logger.debug("Fixed test %s removed from the fails file", test_name)
        else:
            logger.debug("Fixed test %s is not present in the fails file", test_name)


def __patch_content(
    index: git.index.base.IndexFile,
    file_name: str,
    contents: list[str],
    add_index: bool,
) -> None:
    """
    Once the file's content is ready, write in the file and add it to the git
    repo to prepare it to be committed.
    :param index: gitpython object to control the commit
    :param file_name: string
    :param contents: list of lines
    :param add_index: flag to include the change to a near commit
    :return:
    """
    with open(file_name, "wt") as file_descriptor:
        file_descriptor.writelines(contents)
    logger.info("Patched file %s", file_name)
    if add_index:
        index.add(file_name)


def __get_file_content(file_name: str) -> list[str]:
    """
    Return the lines in a file as a list.
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        with open(file_name, "rt") as file_descriptor:
            return file_descriptor.readlines()
    else:
        return list()


def push_to_merge_request(
    repo: git.Repo,
    failures: dict[str, dict[str, deque[str]]],
    pipeline: ProjectPipeline,
) -> None:
    """
    Once we have a viable candidate to uprev, generate or append to an
    existing merge request the proposal. It can be with or without an update
    on the expectations. Once the merge request has a pipeline to verify this
    proposal, the tool triggers it.
    :param repo: git.Repo
    :param failures: dict
    :param pipeline: ProjectPipeline
    :return: None
    """
    push_to_the_fork(repo, wait_pipeline_creation=False)
    regressions, flakes, unexpectedpass = __get_failures_by_category(failures)
    note_comments = __prepare_comments(pipeline, regressions, flakes, unexpectedpass)
    merge_request = __publish_uprev_merge_request()
    __append_note_to_merge_request(merge_request, note_comments)
    __run_merge_request_pipeline(merge_request, repo.head.commit.hexsha)


def __get_failures_by_category(
    failures: dict[str, dict[str, deque[str]]]
) -> Tuple[
    dict[str, dict[str, str]],
    dict[str, dict[str, list[str]]],
    dict[str, dict[str, str]],
]:
    """
    From the collection of failures, classify them by category if there are
    regressions or flakes.
    :param failures: dict
    :return: Tuple[regressions, flakes, unexpectedpass]
    """
    regressions_per_category: dict[str, dict[str, str]] = defaultdict(dict)
    flake_per_category: dict[str, dict[str, list[str]]] = defaultdict(dict)
    unexpectedpass_per_category: dict[str, dict[str, str]] = defaultdict(dict)
    for job_name, test_dict in failures.items():
        expectations = __get_expectations_path_and_file(job_name)
        if expectations is None:
            alert = " (Missing expectations update in the proposal)."
        else:
            alert = ""
        for test_name, results in test_dict.items():
            results_unique = set(results)
            if len(results_unique) != 1:
                category = f"Unreliable tests on {job_name}{alert}:\n"
                flake_per_category[category][test_name] = list(results)
            elif results[0] not in ["UnexpectedPass", "UnexpectedImprovement(Pass)"]:
                category = f"Possible regressions on {job_name}{alert}:\n"
                regressions_per_category[category][test_name] = results[0]
            else:
                assert len(results_unique) == 1
                assert results[0] in ["UnexpectedPass", "UnexpectedImprovement(Pass)"]
                category = f"Fix test with the uprev on {job_name}{alert}:\n"
                unexpectedpass_per_category[category][test_name] = results[0]
    return regressions_per_category, flake_per_category, unexpectedpass_per_category


def __prepare_comments(
    pipeline: ProjectPipeline,
    regressions_per_category: dict[str, dict[str, str]],
    flake_per_category: dict[str, dict[str, list[str]]],
    unexpectedpass_per_category: dict[str, dict[str, str]],
) -> Tuple[list[str], list[str], list[str], list[str]]:
    """
    With the information available, prepare the list of comments to be added
    in a note in the merge request.
    :param pipeline: ProjectPipeline
    :param regressions_per_category: dict
    :param flake_per_category: dict
    :param unexpectedpass_per_category: dict
    :return: list
    """
    comments = (
        __source_comment(pipeline),
        __generate_comment(regressions_per_category, False),
        __generate_comment(flake_per_category, True),
        __generate_comment(unexpectedpass_per_category, False),
    )
    return comments


def __publish_uprev_merge_request() -> ProjectMergeRequest:
    """
    Check if a merge request is already in progress or create a newer one.
    :return: ProjectMergeRequest
    """
    fork_project = Fork().gl_project
    uprev_branch = Branch().for_merge_request
    target_project = get_target_project().gl_project
    mr_title = Title().for_merge_request
    merge_request = __get_uprev_merge_request(mr_title)

    if merge_request is None:
        labels = __get_uprev_labels()
        merge_request = fork_project.mergerequests.create(  # type: ignore
            {
                "source_branch": uprev_branch,
                "target_branch": target_project.default_branch,
                "target_project_id": target_project.id,
                "title": mr_title,
                "labels": labels,
            }
        )
        logger.info("Created merge request %s", merge_request.web_url)  # type: ignore
    else:
        logger.info("Pushed to existing merge request %s", merge_request.web_url)
    __link_merge_request_with_issue(merge_request)  # type: ignore
    return merge_request  # type: ignore
    # mypy error, but it cannot return None if __get_uprev_merge_request returns None,
    # it will create a fresh one.


@cache
@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
# retry waiting 1, 2, 4, 8, 16, 32, 64, 128, 180... > 10 minutes
def __get_uprev_merge_request(title: str) -> Optional[ProjectMergeRequest]:
    gl_user = GitlabProxy().gl_obj.user
    username = gl_user.username if gl_user is not None else ""
    uprev_branch = Branch().for_merge_request
    target_project = get_target_project().gl_project
    query = {
        "state": "opened",
        "author_username": username,
        "search": title,
        "source_branch": uprev_branch,
    }
    try:
        mr = next(
            iter(
                target_project.mergerequests.list(query_parameters=query, iterator=True)
            )
        )
        return target_project.mergerequests.get(mr.iid)
    except StopIteration:
        # to scape tenacity when the exception is because there isn't such MR.
        return None


def __link_merge_request_with_issue(merge_request: ProjectMergeRequest) -> None:
    issue = __search_existing_uprev_issue()
    if isinstance(issue, ProjectIssue):
        description = [
            f"Uprev candidate generated in merge request {merge_request.web_url}. The issue is solved."
        ]
        issue.notes.create({"body": "".join(description)})
        if in_production():
            try:
                issue.state_event = "close"
                issue.save()
            except GitlabUpdateError as exception:
                logger.error(
                    "Exception '%s' when closing the issue about previous problems to uprev. See %s",
                    exception.error_message,
                    issue.web_url,
                )
    else:
        logger.debug(
            "There isn't a previously in progress issue about this uprev to "
            "report this new uprev candidate."
        )


def __append_note_to_merge_request(
    merge_request: ProjectMergeRequest,
    comments: Tuple[list[str], list[str], list[str], list[str]],
) -> None:
    """
    With new or in-progress merge requests, add a note with information about
    the current proposal to uprev.
    :param merge_request: ProjectMergeRequest
    :param comments: list
    :return:
    """
    _comments: list[str] = ["".join(c) for c in comments if c]
    merge_request.notes.create({"body": "\n".join(_comments)})


def __run_merge_request_pipeline(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> None:
    """
    Once the merge request has the information about the uprev proposal, a
    pipeline is created to verify that the proposal doesn't brake previous
    work. Creating, triggering, and monitoring the evolution is the last part
    of the tasks of this tool.
    :param merge_request: ProjectMergeRequest
    :return: None
    """
    merge_request = __wait_until_merge_request_commit_matches(
        merge_request, commit_hash
    )
    # Depending on the groups the user belongs, this merge request could have
    # the pipeline in the fork or in the main project. So, we must check where
    # the information we have points to continue the process.
    merge_request = __wait_commits_merge_request_head_pipeline(
        merge_request, commit_hash
    )
    merge_request = __check_merge_request_source(merge_request)
    __check_merge_request_pipeline_project(merge_request)
    merge_request_pipeline = __wait_pipeline_creation(
        commit_hash, source="merge_request_event"
    )
    failures = run_pipeline(merge_request_pipeline)
    if merge_request_pipeline.status == "failed" or "failed" in {
        job.status
        for job in merge_request_pipeline.jobs.list(
            iterator=True, include_retried=False
        )
    }:
        # Check if the status is failed. In some cases we may see manual but
        # some job in failed status. So first do the quick check because the
        # second could be avoided if the first is already true. It is the
        # default, but explicitly exclude the retried, because the interest is
        # in the final result.
        msg = f"Something wrong in the verification pipeline {merge_request_pipeline.web_url}"
        if failures:
            logger.critical("%s\n%s", msg, pformat(failures))
        raise RuntimeError(msg)
    elif failures:
        logger.warning("Flaky verification pipeline\n%s", pformat(failures))


def __wait_until_merge_request_commit_matches(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the merge request points to the expected
    commit hash.
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.sha == commit_hash:
            logger.info(
                "Merge request with latest commit %s. Look for its merge request pipeline!",
                merge_request.sha,
            )
            return merge_request
        if first_attempt:
            logger.info("Merge request doesn't have yet the latest commit. Wait...")
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __wait_commits_merge_request_head_pipeline(
    merge_request: ProjectMergeRequest, commit_hash: str
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request points
    to the expected commit hash.
    :param merge_request: merge request where the commit was pushed
    :param commit_hash: sha1 of the commit previously pushed
    :return: refreshed merge request object
    """
    first_attempt_of_none = True
    first_attempt_of_sha = True
    while True:
        if merge_request.head_pipeline is not None:
            if merge_request.head_pipeline["sha"] == commit_hash:
                logger.info(
                    "Merge request head pipeline %s points to the latest commit %s.",
                    merge_request.head_pipeline["id"],
                    merge_request.head_pipeline["sha"],
                )
                return merge_request
            if first_attempt_of_sha:
                logger.info(
                    "Merge request head pipeline %s doesn't point yet to the latest commit. Wait...",
                    merge_request.head_pipeline["id"],
                )
                first_attempt_of_sha = False
        elif first_attempt_of_none:
            logger.info("Merge request doesn't have yet a head pipeline. Wait...")
            first_attempt_of_none = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_source(
    merge_request: ProjectMergeRequest,
) -> ProjectMergeRequest:
    """
    This method will wait until the head pipeline of the merge request
    corresponds to one from the merge request event.
    :param merge_request: merge request where the uprev was pushed
    :return: refreshed merge request object
    """
    first_attempt = True
    while True:
        if merge_request.head_pipeline["source"] == "merge_request_event":
            logger.info(
                "Head pipeline %s is from a merge request event (%s).",
                merge_request.head_pipeline["id"],
                merge_request.head_pipeline["source"],
            )
            return merge_request
        if first_attempt:
            logger.info(
                "Head pipeline %s doesn't point yet to merge request event. Wait...",
                merge_request.head_pipeline["id"],
            )
            first_attempt = False
        time.sleep(CHECKER_SLEEP_TIME)
        merge_request = __get_merge_request(merge_request.iid)


def __check_merge_request_pipeline_project(merge_request: ProjectMergeRequest) -> None:
    """
    Depending on the rights of the user running the uprev, the merge request
    pipeline is created in the main project or in the fork. So, this method
    checks in which of them is created and configures the tool to proceed
    accordingly.
    :param merge_request: merge request where the uprev was pushed
    :return:
    """
    project_id = merge_request.head_pipeline["project_id"]
    project = GitlabProxy().gl_obj.projects.get(project_id)
    logger.info(
        "Merge request with the pipeline %s from the latest commit (in %s). Run this pipeline! %s",
        merge_request.head_pipeline["id"],
        project.path_with_namespace,
        merge_request.head_pipeline["web_url"],
    )
    if Target().gl_project == project:
        Working().project = Target()
    elif Fork().gl_project == project:
        Working().project = Fork()
    else:
        raise RuntimeError(
            f"The merge request pipeline is in project {project.path_with_namespace}, not the Target or the Fork."
        )


@retry(
    stop=stop_after_attempt(10),
    wait=wait_exponential(multiplier=1, max=180),
    after=after_log(logger, ERROR),
)
def __get_merge_request(mergerequest_id: int) -> ProjectMergeRequest:
    """
    Get a merge request object from the target project. If there is an
    exception, use tenacity to retry.
    A practical use of this method is to refresh the gitlab object.
    :param mergerequest_id: merge request identifier
    :return:
    """
    return get_target_project().gl_project.mergerequests.get(mergerequest_id)


def __source_comment(pipeline: ProjectPipeline) -> list[str]:
    """
    With environment information, prepare a few lines for the merge request
    note reporting how this proposal has been produced.
    :param pipeline: ProjectPipeline
    :return: lines as list
    """
    job_id = os.getenv("CI_JOB_ID")
    if job_id is not None:
        gl = GitlabProxy().gl_obj
        ci_project_id = os.getenv("CI_PROJECT_ID")
        if ci_project_id is None:
            source_comment = [
                f"This information comes from the ci-uprev job {job_id}.\n"
            ]
        else:
            uprev_project = gl.projects.get(ci_project_id)
            uprev_job = uprev_project.jobs.get(job_id)
            source_comment = [
                f"This information comes from the execution of ci-uprev in job [{uprev_job.id}]({uprev_job.web_url}).\n"
            ]
    else:
        source_comment = [
            "This information comes from a local execution of ci-uprev.\n"
        ]
    pipelines_summary = f"Related pipeline: [{pipeline.id}]({pipeline.web_url})"
    pipelines_summary += ".\n"
    source_comment += [pipelines_summary]
    dependency_diff = Dependency().diff
    if dependency_diff:
        source_comment.append(
            f"\n\nThe dependency changes can be reviewed with this compare: {dependency_diff}.\n"
        )
    return source_comment


def __generate_comment(
    by_category: dict[str, dict[str, str]] | dict[str, dict[str, list[str]]],
    as_list: bool,
) -> list[str]:
    """
    Transform the dictionary with the information by category to the format we
    place this report in the merge request note.
    :param by_category: dict
    :param as_list: bool
    :return: lines as list
    """
    comment = []
    for category, problem in by_category.items():
        category_comment = [category]
        for test_name, result in problem.items():
            if as_list:
                line = f"- {test_name}, {list(result)}\n"
            else:
                line = f"- {test_name}, {result}\n"
            category_comment.append(line)
        category_comment = __truncate_long_messages(category_comment)
        category_comment.append("\n")
        comment.extend(category_comment)
    return comment


def __truncate_long_messages(messages: list[str]) -> list[str]:
    """
    When a message list of lines is too long and could disturb the reading of
    the user, this method packs the lines over a limit in a details Markdown
    code.
    :param messages: lines in a message
    :return: original or, if needed, truncated message
    """
    if len(messages) > LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:
        messages, details, excedent = (
            messages[:LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE],
            messages[
                LINES_PER_CATEGORY_TO_SUMMARY_IN_NOTE:MAX_LINES_PER_CATEGORY_IN_NOTE
            ],
            messages[MAX_LINES_PER_CATEGORY_IN_NOTE:],
        )
        messages.append("\n\n<details>\n")
        messages.append("\n<summary>... and more</summary>\n\n")
        messages.extend(details)
        if excedent:
            # TODO: link to the commit content
            messages.append("\nToo much, to see more check the file in the commit\n")
        messages.append("\n</details>\n")
    return messages


def main() -> None:
    args = __cli_arguments()
    __setup_logging()

    try:
        __cmd_uprev(args.revision, args.pipeline)
    except NothingToUprev:
        logger.info(
            "Latest commit in the dependency project is the revision "
            "already in use in the target project. No need for uprev."
        )
        return


def __cli_arguments() -> Namespace:
    """
    Define the command line behavior for the arguments and subcommands.
    :return: argument parser
    """
    parser = ArgumentParser(
        description="CLI tool to update the revision of a project used "
        "inside another project."
    )
    # TODO: mention that no argument is required
    #  all the configuration elements come from environment variables and this
    #  arguments are only for development and debugging.
    #  So, also show help about those envvars
    #   TARGET_PROJECT_PATH, FORK_PROJECT_PATH, DEP_PROJECT_PATH, GITLAB_TOKEN
    parser.add_argument(
        "--revision", metavar="HASH", help="Specify the revision to uprev."
    )
    parser.add_argument(
        "--pipeline",
        metavar="PIPELINE_ID",
        help="Specify the pipeline to reference in the uprev. Requires to have specified the revision.",
    )
    return parser.parse_args()


def __setup_logging() -> None:
    def logfilename() -> str:
        results = Path("./results")
        if not results.exists():
            results.mkdir()
        if results.is_dir():
            file = Path(results, "ci-uprev.log")
        else:
            file = Path("ci-uprev.log")
        return "/".join(file.parts)

    file_handler = FileHandler(filename=logfilename())
    console_handler = StreamHandler(stream=sys.stdout)
    file_handler.formatter = Formatter(
        "%(asctime)s - %(levelname)s - %(threadName)s - %(message)s"
    )
    console_handler.formatter = colorlog.ColoredFormatter(
        "%(log_color)s%(message)s",
        log_colors={
            "DEBUG": "blue",
            "INFO": "white",
            "WARNING": "yellow",
            "ERROR": "red",
            "critical": "red,bg_white",
        },
    )

    logger.setLevel(DEBUG)
    file_handler.level = DEBUG
    console_handler.level = INFO

    logger.addHandler(file_handler)
    logger.addHandler(console_handler)


def __cmd_uprev(
    revision: str | None,
    pipeline_id: int | None,
) -> None:
    """
    This is the default procedure of the uprev tool. Without options, the
    default, it will search for a pipeline in the project to uprev that
    satisfies the necessary conditions. The commit of this pipeline will be
    used to uprev in the target project. (Those two variables, revision and
    pipeline, can be fixed using option arguments for debug purposes like
    repeat and compare with a previous execution.)

    Then the uprev candidate is tested, if there are expectations to update
    they are and the commit amended, to then generate a merge request proposal.

    :param revision: optional parameter to specify the revision to use
    :param pipeline_id: optional parameter to specify the pipeline to use
    :return:
    """
    revision, pipeline_id = get_candidate_for_uprev(revision, pipeline_id)
    uprev_pair = UpdateRevisionPair().enum
    if uprev_pair == UpdateRevision.mesa_in_virglrenderer:
        templates_commit = get_templates_commit(revision)
    elif uprev_pair == UpdateRevision.piglit_in_mesa:
        templates_commit = get_templates_commit("main")
    else:
        raise NotImplementedError(f"Not yet implemented the uprev of {uprev_pair.name}")
    repo = create_branch(pipeline_id, revision, templates_commit)
    pipeline = push_to_the_fork(repo)
    if pipeline is None:
        raise AssertionError("Push to the fork didn't produce a pipeline")
    failures = run_pipeline(pipeline)
    update_branch(repo, failures)
    push_to_merge_request(repo, failures, pipeline)


def __search_pipeline(revision: str) -> int:
    """
    When a revision is specified, the pipeline it has generated is needed.
    :param revision: dep project revision
    :return: pipeline id
    """
    dep_project: Project = Dependency().gl_project
    pipelines = list(dep_project.pipelines.list(sha=revision, state="success"))
    if not pipelines:
        raise ValueError("No pipeline found with this revision")
    return int(pipelines[-1].id)


def __get_pipeline_commit(pipeline_id: int) -> str:
    """
    When a pipeline is specified, the revision that generated it is needed.
    :param pipeline_id: identification number
    :return: string with the commit sha
    """
    pipeline = __get_pipeline(pipeline_id, project=Dependency().gl_project)
    return str(pipeline.sha)


if __name__ == "__main__":
    main()
