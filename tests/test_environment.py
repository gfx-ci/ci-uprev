#!/usr/bin/env python3.11

# Copyright (C) 2023-2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from enum import Enum
from logging import getLogger
from os import environ
from pathlib import Path
from unittest import TestCase

import responses
from common import clean_singletons
from gitlab import Gitlab
from gitlab.v4.objects import Project

from uprev.environment import (
    Branch,
    Dependency,
    Fork,
    GitlabProxy,
    LocalClone,
    Target,
    Template,
    Title,
    UpdateRevisionPair,
    Working,
    _InProduction,
    _UprevProject,
    get_target_project,
)
from uprev.uprevs import UpdateRevision

TEST_ROOT = Path(__file__).parent / "environment"
RECORD_FILE = "test_environment%s.yaml"
GITLAB_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_GitLab"))
TARGET_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Target"))
FORK_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Fork"))
GET_TARGET_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_get_target"))
DEPENDENCY_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Dependency"))
WORKING_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Working"))
TEMPLATE_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Template"))
UPDATEREVISIONPAIR_RECORD_FILE = str(
    TEST_ROOT / str(RECORD_FILE % "_UpdateRevisionPair")
)
LOCALCLONE_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_LocalClone"))
BRANCH_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Branch"))
TITLE_RECORD_FILE = str(TEST_ROOT / str(RECORD_FILE % "_Title"))

TARGET_NAMESPACE = "mesa"
TARGET_PROJECT = "piglit"
FORK_PATH_WITH_NAMESPACE = "sergi/mesa"
MESA_PATH_WITH_NAMESPACE = "mesa/mesa"
PIGLIT_PATH_WITH_NAMESPACE = "mesa/piglit"

logger = getLogger(__name__)


class TestEnvironment(TestCase):
    def test_gitlabproxy_singleton(self) -> None:
        clean_gitlabproxy_singleton()

        logger.info(f"Responses coming from {GITLAB_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GITLAB_RECORD_FILE)

            glproxy, gl_obj = request_gitlabproxy_singleton()

        self.assertIsInstance(gl_obj, Gitlab)

    def test_target_singleton(self) -> None:
        clean_singletons([GitlabProxy, Target])

        path_with_namespace = f"{TARGET_NAMESPACE}/{TARGET_PROJECT}"
        environ["TARGET_PROJECT_PATH"] = path_with_namespace

        logger.info(f"Responses coming from {TARGET_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(TARGET_RECORD_FILE)

            path_with_namespace, namespace, path = request_target_singleton()

        self.assertEqual(path_with_namespace, path_with_namespace)
        self.assertEqual(namespace, TARGET_NAMESPACE)
        self.assertEqual(path, TARGET_PROJECT)

    def test_fork_singleton(self) -> None:
        clean_fork_singleton()

        environ["FORK_PROJECT_PATH"] = FORK_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {FORK_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(FORK_RECORD_FILE)

            fork_path_with_namespace = request_fork_singleton()

        self.assertEqual(fork_path_with_namespace, FORK_PATH_WITH_NAMESPACE)

    # def test_fork_singleton_envvar_not_defined(self) -> None:
    # TODO: undefined environment variable FORK_PROJECT_PATH to use the user's fork

    def test_get_target(self) -> None:
        clean_get_target_singleton()

        environ["PRODUCTION"] = "true"
        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {GET_TARGET_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(GET_TARGET_RECORD_FILE)

            target, target_path_with_namespace = request_get_target_singleton()

        self.assertIsInstance(target, Target)
        self.assertEqual(target_path_with_namespace, MESA_PATH_WITH_NAMESPACE)

    def test_dependency_singleton(self) -> None:
        clean_dependency_singleton()

        environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {DEPENDENCY_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(DEPENDENCY_RECORD_FILE)

            path_with_namespace = request_dependency_singleton()

        self.assertEqual(path_with_namespace, PIGLIT_PATH_WITH_NAMESPACE)

    def test_working_project_singleton(self) -> None:
        clean_working_singleton()

        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {WORKING_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(WORKING_RECORD_FILE)

            working_gl_project, target_gl_project = request_working_singleton()

        self.assertEqual(working_gl_project, target_gl_project)

    def test_template_singleton(self) -> None:
        clean_template_singleton()

        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {TEMPLATE_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(TEMPLATE_RECORD_FILE)

            template_gl_project, target_gl_project = request_template_singleton()

        self.assertEqual(template_gl_project, target_gl_project)

    def test_updaterevisionpair_singleton(self) -> None:
        clean_updaterevisionpair_singleton()

        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
        environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {UPDATEREVISIONPAIR_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(UPDATEREVISIONPAIR_RECORD_FILE)

            uprev_pair_enum = request_uprevrevisionpair_singleton()

        self.assertEqual(uprev_pair_enum, UpdateRevision.piglit_in_mesa)

    def test_localclone_singleton(self) -> None:
        clean_localclone_singleton()

        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE

        logger.info(f"Responses coming from {LOCALCLONE_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(LOCALCLONE_RECORD_FILE)

            directory = request_localclone_singleton()

        self.assertEqual(directory, "./tmp_mesa.git")

    def test_branch_singleton(self) -> None:
        clean_branch_singleton()

        environ["TARGET_PROJECT_PATH"] = MESA_PATH_WITH_NAMESPACE
        environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
        environ["PRODUCTION"] = "true"

        logger.info(f"Responses coming from {BRANCH_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(BRANCH_RECORD_FILE)

            for_merge_request, for_wip = request_branch_singleton()

        self.assertEqual(for_merge_request, "uprev-piglit")
        self.assertEqual(for_wip, "uprev-piglit-tmp")

    def test_title_singleton(self) -> None:
        clean_title_singleton()

        environ["DEP_PROJECT_PATH"] = PIGLIT_PATH_WITH_NAMESPACE
        environ["PRODUCTION"] = "true"

        logger.info(f"Responses coming from {TITLE_RECORD_FILE}")
        with responses.RequestsMock() as rsps:
            rsps._add_from_file(TITLE_RECORD_FILE)

            for_merge_request, for_issue = request_title_singleton()

        self.assertEqual(for_merge_request, "ci: Uprev Piglit")
        self.assertEqual(for_issue, "ci: Uprev Piglit failed")


def clean_gitlabproxy_singleton() -> None:
    clean_singletons([GitlabProxy])


def request_gitlabproxy_singleton() -> tuple[GitlabProxy, Gitlab]:
    glproxy = GitlabProxy()
    return glproxy, glproxy.gl_obj


def clean_target_singleton() -> None:
    clean_singletons([GitlabProxy, Target])


def request_target_singleton() -> tuple[str, str, str]:
    target = Target()
    return target.path_with_namespace, target.namespace, target.path


def clean_fork_singleton() -> None:
    clean_singletons([GitlabProxy, Fork])


def request_fork_singleton() -> str:
    fork = Fork()
    return fork.path_with_namespace


def clean_get_target_singleton() -> None:
    clean_singletons([GitlabProxy, Target, Fork, _InProduction])


def request_get_target_singleton() -> tuple[_UprevProject, str]:
    target = get_target_project()
    return target, target.path_with_namespace


def clean_dependency_singleton() -> None:
    clean_singletons([GitlabProxy, Dependency])


def request_dependency_singleton() -> str:
    dependency = Dependency()
    return dependency.path_with_namespace


def clean_working_singleton() -> None:
    clean_singletons([GitlabProxy, Target, Working])


def request_working_singleton() -> tuple[Project | None, Project]:
    target = Target()
    Working().project = target
    return Working().gl_project, target.gl_project


def clean_template_singleton() -> None:
    clean_singletons([GitlabProxy, Target, Template])


def request_template_singleton() -> tuple[Project | None, Project]:
    target = Target()
    Template().project = target
    return Template().gl_project, target.gl_project


def clean_updaterevisionpair_singleton() -> None:
    clean_singletons([GitlabProxy, Target, UpdateRevisionPair])


def request_uprevrevisionpair_singleton() -> Enum:
    return UpdateRevisionPair().enum


def clean_localclone_singleton() -> None:
    clean_singletons([GitlabProxy, Target, LocalClone])


def request_localclone_singleton() -> str:
    return str(LocalClone().directory)


def clean_branch_singleton() -> None:
    clean_singletons([GitlabProxy, Dependency, Branch, _InProduction])


def request_branch_singleton() -> tuple[str, str]:
    branch = Branch()
    return str(branch.for_merge_request), str(branch.for_wip)


def clean_title_singleton() -> None:
    clean_singletons([GitlabProxy, Dependency, Title])


def request_title_singleton() -> tuple[str, str]:
    title = Title()
    return title.for_merge_request, title.for_issue
