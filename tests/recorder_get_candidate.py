#!/usr/bin/python3 -B

# Copyright (C) 2025 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2025 Collabora Ltd"

import json
import sys
from logging import DEBUG, StreamHandler, basicConfig, getLogger
from os import environ

from recorder_environment import post_process_yaml
from responses import _recorder
from test_get_candidate import (
    DEP_PROJECT_PATH,
    GET_FROM_BOTH,
    GET_FROM_PIPELINE_ID,
    GET_FROM_REVISION,
    GET_THE_LAST,
    GET_WRONG_PAIR,
    JSON_FILE,
    TARGET_PROJECT_PATH,
    UNRELATED_PIPELINE_ID,
    clean_get_candidate_from_both,
    clean_get_candidate_from_pipeline,
    clean_get_candidate_from_revision,
    clean_get_candidate_from_wrong_pair,
    clean_get_candidate_latest,
)

from uprev.main import get_candidate_for_uprev

logger = getLogger(__name__)


def prepare_recording() -> None:
    __setup_logging()
    environ["TARGET_PROJECT_PATH"] = TARGET_PROJECT_PATH
    environ["DEP_PROJECT_PATH"] = DEP_PROJECT_PATH


def record_get_candidate() -> None:
    revision, pipeline_id = record_get_candidate_latest()
    record_get_candidate_from_revision(revision)
    record_get_candidate_from_pipeline(pipeline_id)
    record_get_candiadte_from_both(revision, pipeline_id)
    record_wrong_pair(revision)
    generate_json(revision, pipeline_id)


def record_get_candidate_latest() -> tuple[str, int]:
    logger.debug("======== record_get_candidate_latest ========")
    clean_get_candidate_latest()
    revision, pipeline_id = recorder_get_the_latest()
    logger.info(f"Generated {GET_THE_LAST}")
    post_process_yaml(GET_THE_LAST)
    return revision, pipeline_id


@_recorder.record(file_path=GET_THE_LAST)  # type: ignore [misc]
def recorder_get_the_latest() -> tuple[str, int]:
    return get_candidate_for_uprev(None, None)


def record_get_candidate_from_revision(revision: str) -> None:
    logger.debug("======== record_get_candidate_from_revision ========")
    clean_get_candidate_from_revision()
    recorder_from_revision(revision)
    logger.info(f"Generated {GET_FROM_REVISION}")
    post_process_yaml(GET_FROM_REVISION)


@_recorder.record(file_path=GET_FROM_REVISION)  # type: ignore [misc]
def recorder_from_revision(_revision: str) -> None:
    get_candidate_for_uprev(_revision, None)


def record_get_candidate_from_pipeline(pipeline_id: int) -> None:
    logger.debug("======== record_get_candidate_from_pipeline ========")
    clean_get_candidate_from_pipeline()
    recorder_from_pipeline_id(pipeline_id)
    logger.info(f"Generated {GET_FROM_PIPELINE_ID}")
    post_process_yaml(GET_FROM_PIPELINE_ID)


@_recorder.record(file_path=GET_FROM_PIPELINE_ID)  # type: ignore [misc]
def recorder_from_pipeline_id(_pipeline_id: int) -> None:
    get_candidate_for_uprev(None, _pipeline_id)


def record_get_candiadte_from_both(revision: str, pipeline_id: int) -> None:
    logger.debug("======== record_get_candiadte_from_both ========")
    clean_get_candidate_from_both()
    recorder_from_both(revision, pipeline_id)
    logger.info(f"Generated {GET_FROM_BOTH}")
    post_process_yaml(GET_FROM_BOTH)


@_recorder.record(file_path=GET_FROM_BOTH)  # type: ignore [misc]
def recorder_from_both(_revision: str, _pipeline_id: int) -> None:
    get_candidate_for_uprev(_revision, _pipeline_id)


def record_wrong_pair(revision: str) -> None:
    logger.debug("======== record_wrong_pair ========")
    clean_get_candidate_from_wrong_pair()
    recorder_wrong_pair(revision, UNRELATED_PIPELINE_ID)
    logger.info(f"Generated {GET_WRONG_PAIR}")
    post_process_yaml(GET_WRONG_PAIR)


@_recorder.record(file_path=GET_WRONG_PAIR)  # type: ignore [misc]
def recorder_wrong_pair(_revision: str, _pipeline_id: int) -> None:
    try:
        get_candidate_for_uprev(_revision, _pipeline_id)
    except AssertionError:
        pass
    else:
        raise RuntimeError("It must have failed!")


def __setup_logging() -> None:
    basicConfig(format="%(asctime)s - %(levelname)s - %(message)s", level=DEBUG)
    console_handler = StreamHandler(stream=sys.stdout)
    console_handler.level = DEBUG


def generate_json(revision: str, pipeline_id: int) -> None:
    get_candidate = {"revision": revision, "pipeline_id": pipeline_id}
    with open(JSON_FILE, mode="w", encoding="utf-8") as write_file:
        json.dump(get_candidate, write_file)


def main() -> None:
    prepare_recording()
    record_get_candidate()


if __name__ == "__main__":
    main()
