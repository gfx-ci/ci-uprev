#!/usr/bin/python3 -B

# Copyright (C) 2024 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT
from __future__ import annotations

from functools import partial
from pathlib import Path
from subprocess import PIPE, Popen
from subprocess import run as _run

PROJECT_ROOT = Path(__file__).parent.parent
run = partial(_run, check=True, cwd=PROJECT_ROOT)

# Start spell checking since this commit
KNOWN_GOOD_COMMIT = "d7598cc2f950e995e2d984bdaba56d513ae319f8"
IGNORED_WORDS = (
    "marge",
    "zink",
)
CODESPELL_BASE_ARGS = (
    "codespell",
    "--context",
    "3",
    "--ignore-words-list",
    ",".join(IGNORED_WORDS),
    "--skip",
    ".git",
)
SKIP_FILES_GLOB = ",".join(
    ("*-fails.txt", "*-flakes.txt", "*-skips.txt", "test*.yaml", "tmp*", ".mypy*")
)


def main() -> None:
    run((*CODESPELL_BASE_ARGS, "--skip", SKIP_FILES_GLOB, "--check-filenames", "."))

    with Popen(
        args=(
            "git",
            "log",
            "--max-count=50",
            "--no-merges",
            "--format='%H%n%n%s%n%n%b'",
            f"{KNOWN_GOOD_COMMIT}..",
        ),
        cwd=PROJECT_ROOT,
        stdout=PIPE,
    ) as git_log:
        run(
            args=(
                *CODESPELL_BASE_ARGS,
                "-",
            ),
            stdin=git_log.stdout,
            timeout=5,
        )


if __name__ == "__main__":
    main()
